import requests, json

def get(url, params = None, **kwargs):
    headers = {
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'}
    response = requests.get(url, params, headers=headers)
    try:
        if response.status_code == 200:
            response_content = response.content
            response_content = json.loads(response.content)
            # if kwargs is not None and kwargs.get('content_type', None) == 'json':
            #     response_content = json.loads(response.content)
            return response_content
            # if success_callback is not None:

        else:
            return None
    except Exception as e:
        print("请求发生错误", "url:", url, "错误", e)
        return None

def redirect(url):
    # 请求头，这里我设置了浏览器代理
    headers = {'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'}
    # 请求网页
    response = requests.get(url, headers=headers, allow_redirects=True)
    # print(response.status_code) # 打印响应的状态码
    # print(response.url)  # 打印重定向后的网址
    return response.url