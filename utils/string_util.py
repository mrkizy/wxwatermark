import re, os
from urllib import parse


def find_url(string):
    # findall() 查找匹配正则表达式的字符串
    urls = re.findall(r"(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*,]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)|([a-zA-Z]+.\w+\.+[a-zA-Z0-9\/_]+)",string)
    urls = list(sum(urls, ()))
    urls = [x for x in urls if x != '']
    if not urls and len(urls) <= 0:
        return None
    return urls[0]


def pharse_last_path(url):
    if not url:
        return None
    url_obj = parse.urlparse(url)
    url_path = url_obj.path
    if not url_path:
        return None
    paths = url_path.split("/")
    return paths[-2]
