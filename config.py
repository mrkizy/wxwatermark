import os

# 是否开启debug模式
DEBUG = True

# 临时文件储存目录
FILE_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'files')
if not os.path.exists(FILE_DIR):
    os.mkdir(FILE_DIR)

# 读取数据库环境变量
username = os.environ.get("MYSQL_USERNAME", 'root')
password = os.environ.get("MYSQL_PASSWORD", 'Xuzhi4972569@2022')
db_address = os.environ.get("MYSQL_ADDRESS", '127.0.0.1:3306')
