from network import http
import utils, os
import urllib, hashlib
from flask import render_template, request, jsonify, send_file
from run import app
from wxcloudrun.response import make_succ_empty_response, make_succ_response, make_err_response

@app.get('/api/video')
def get_video_info():
    share_url = request.args.get("share_url")
    if not share_url:
        return make_err_response("参数为空")
    real_url = utils.string_util.find_url(share_url)
    if not real_url:
        return make_err_response("无效参数")
    site_type = 0  # 1 抖音 2 快手
    if "v.douyin.com" in real_url:
        site_type = 1
    elif "v.kuaishou.com" in real_url:
        site_type = 2
    redirect_url = http.redirect(real_url)
    if not redirect_url:
        return make_err_response("无效地址", 400)

    if site_type == 1:
        return pharse_douyin(redirect_url)
    elif site_type == 2:
        return pharse_kwai(redirect_url)

    return make_succ_empty_response()

@app.get('/api/video/download')
def upload_video():
    video_url = request.args.get("video_url")
    if not video_url:
        return make_err_response('下载错误')
    myMd5 = hashlib.md5()
    myMd5.update(video_url.encode('utf-8'))
    myMd5_Digest = myMd5.hexdigest()
    video_path = '{}/{}.mp4'.format(app.config['FILE_DIR'], myMd5_Digest)
    urllib.request.urlretrieve(video_url, video_path)

    return send_file(video_path, as_attachment=True)


# 抖音
def pharse_douyin(url):
    awe_id = utils.string_util.pharse_last_path(url)
    if not awe_id:
        return make_err_response("无效地址")

    result = http.get("https://www.iesdouyin.com/web/api/v2/aweme/iteminfo", params={"item_ids": awe_id})
    if not result:
        return make_err_response("解析失败")

    item_list = result.get("item_list", None)
    if not item_list or len(item_list) <= 0:
        return make_succ_empty_response()

    item = item_list[0]
    if not item:
        return make_succ_empty_response()
    images = item['images']
    if images and len(images) > 0:
        # 是图片
        image_result = []
        for image in images:
            image_url_list = image.get("url_list", None)
            image_url = image_url_list[0] if image_url_list is not None and len(image_url_list) > 0 else None
            if image_url is not None:
                image_result.append(image_url)
        return make_succ_response({'type': 'image', 'images': image_result})
    else:
        # 是视频
        video_url = result.get("item_list", None)[0]['video']['play_addr']['url_list'][0].replace('playwm', 'play')
        video_url = http.redirect(video_url)
        cover_url = result['item_list'][0]['video']['cover']['url_list'][0]
        desc = result['item_list'][0]['desc']
        return make_succ_response(
            {'type': 'video', 'video_url': video_url, "cover_url": cover_url, "desc": desc})

    # return make_success_response(result), 200


# 快手
def pharse_kwai(url):
    url = http.redirect(url)
    url = http.redirect(url)
    url = http.redirect(url)
    url = http.redirect(url)
    url = http.redirect(url)
    print(url)


# def make_error_response(err_msg, err_code=400):
#     return jsonify({"errMsg": err_msg, "code": err_code})
#
#
# def make_success_response(data):
#     return jsonify({"data": data, "code": 200})